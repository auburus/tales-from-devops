HUGO_VERSION="0.93.2"

.PHONY: help
help:
	@echo "Available commands:"
	@echo "  setenv-ci         Install dependencies for ci server"
	@echo "  pages             Build static site"
	@echo "  dev-server        Start dev server for hugo"

# We need the extended version of hugo for the ananke theme.
.PHONY: setenv-ci
setenv-ci:
	mkdir -p /tmp/get_hugo && \
	cd /tmp/get_hugo && \
	curl -L --fail -o hugo.tar.gz https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz && \
	tar -xzf hugo.tar.gz && \
	mv hugo /usr/local/bin/hugo && \
	cd /tmp && \
	rm -r /tmp/get_hugo

# Need to have the public folder in the top level dir, otherwise gitlab doesn't
# recognize it as valid for publishing to pages...
.PHONY: pages
pages:
	hugo --source pages --destination ../public

.PHONY: dev-server
dev-server:
	hugo --source pages server -D
